from .grid import Grid

class Side:
    
    """ Constructor for a Side """
    def __init__(self, player_name, size):
        self.player_name = player_name
        self.own_grid = Grid(size)
        self.fleet = []
    
    """ Get the player's name """
    def get_player_name(self):
        return self.player_name
    
    """ Get the player's grid """
    def get_own_grid(self):
        return self.own_grid
    
    """ Get the player's aux grid """
    def get_aux_grid(self):
        return self.aux_grid
    
    """ Add ship to fleet """
    def add_ship(self, ship):
        self.fleet.append(ship)

