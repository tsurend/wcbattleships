from .cell import Cell
from .side import Side
from .ship import Ship
from .grid import Grid

class Game:
	
	@staticmethod
	def read_fleet(filename):
		f = open(filename,'r')
		result = dict()
		for l in f:
			r = l.strip().split(',')
			result[r[0]] = int(r[2])
		f.close()
		return result
	
	@staticmethod
	def read_csv(filename):
		f = open(filename,'r')
		for l in f:
			yield(tuple(l.strip().split(',')))
		f.close()
	
	@staticmethod
	def read_layout(side, fleet, filename):
		grid = side.get_own_grid()
		for row, col, name, layout in Game.read_csv(filename):
			row = int(row)
			col = int(col)
			size = fleet[name]
			ship = Ship(name, (row,col), size, layout)
			if layout == 'H':
				d = 0
			else:
				d = 1
			for i in range(size):
				#print(((row,col),i,layout,d,size,(row + i*d, col + i*(1-d))))
				cell = grid.get_cell(row + i*d, col + i*(1-d))
				cell.set_ship(ship)
	
	def __init__(self, fleet_file_name, side_one_position_file_name, side_two_position_file_name):
		self.fleet = self.read_fleet(fleet_file_name)
			
		self.side1 = Side('Player 1',10)
		self.side2 = Side('Player 2',10)
		
		self.read_layout(self.side1,self.fleet,side_one_position_file_name)
		self.read_layout(self.side2,self.fleet,side_two_position_file_name)
		
	
	def move(self,side,row,col):
		cell = side.get_own_grid().get_cell(row,col)
		if cell.get_shot(): # It was shot before
			return  'Reshot'
		ship = cell.get_ship()
		if ship == None:
			return 'Miss'
		num_hits = ship.get_hits()
		ship.set_hits(num_hits+1)
		if ship.get_hits() >= ship.get_size():
			return 'Sunk my ' + ship.get_name()
		if ship.get_hits() > num_hits:
			return 'Hit my ' + ship.get_name()
		
	def run_moves(self,move_file):
		player_num = 0
		sides = [self.side1,self.side2]
		for i,j in self.read_csv(move_file):
			print(self.move(sides[player_num],int(i),int(j)))
			player_num = 1 - player_num


		
		
		
		
