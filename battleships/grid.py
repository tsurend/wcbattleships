from .cell import Cell

class Grid:
    
    """ Constructor for a Grid """
    def __init__(self, size):
        cell_array = []
        for i in range(0, size):
            cell = []
            for j in range(0, size):
                new_cell = Cell(i, j)
                cell.append(new_cell)
            cell_array.append(cell)
        self.cell_array = cell_array
    
    """ Get the grid's cell array """
    def get_cell_array(self):
        return self.cell_array
    
    """ Get a cell from the grid """
    def get_cell(self, i, j):
        return self.cell_array[i-1][j-1]
