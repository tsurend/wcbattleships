import unittest
from battleships import Side
from battleships import Grid
from battleships import Ship

class SideTests(unittest.TestCase):
	
	def test_creation(self):
		sc = Side("Player 1", 12)
		
	def test_val_property(self):
		sc = Side("Player 1", 10)
		
		self.assertEqual(sc.player_name,"Player 1")
		self.assertIsInstance(sc.own_grid,Grid)
		self.assertEqual(sc.fleet,[])
		
	def test_getter(self):
		sc = Side("Player 1", 10)
		
		name = sc.get_player_name()
		owngrid = sc.get_own_grid()

		self.assertEqual(name,sc.player_name)
		self.assertEqual(owngrid,sc.own_grid)

	def test_add_ship(self):
		sc = Side("Player 1", 10)
		
		# Is this where mocking is needed?
		ship1 = Ship('Carrier', [2,3], 3, 'H')
		
		sc.add_ship(ship1)
		self.assertEqual(sc.fleet,[ship1])
		
