import unittest
from battleships import Ship

class ShipTests(unittest.TestCase):
	
	def test_creation(self):
		sc = Ship('Carrier', [1,2], 5, 'H')
		
	def test_val_property(self):
		sc = Ship('Submarine', [2,4], 3, 'V')
		
		self.assertEqual(sc.name,'Submarine')
		self.assertEqual(sc.start_pos,[2,4])
		self.assertEqual(sc.size,3)
		self.assertEqual(sc.orientation,'V')
		self.assertEqual(sc.hits,0)
		
	def test_getter(self):
		sc = Ship('Submarine', [2,4], 3, 'V')
		name = sc.get_name()
		start_pos = sc.get_start_pos()
		size = sc.get_size()
		orientation = sc.get_orientation()
		hits = sc.get_hits()
		
		self.assertEqual(name,sc.name)
		self.assertEqual(start_pos,sc.start_pos)
		self.assertEqual(size,sc.size)
		self.assertEqual(orientation,sc.orientation)
		self.assertEqual(hits,sc.hits)
		
	def test_setter(self):
		sc = Ship('Carrier', [3,4], 6, 'H')
		
		# If we remove the setter methods for name, start_pos, size, orientation, then no need to test the setter methods
		
#		sc.set_name('Battleship')
#		sc.set_start_pos([5,6])
#		sc.set_size(2)
#		sc.set_orientation('V')
		sc.set_hits(5)
		
#		self.assertEqual(sc.name,'Battleship')
#		self.assertEqual(sc.start_pos,[5,6])
#		self.assertEqual(sc.size,2)
#		self.assertEqual(sc.orientation,'V')
		self.assertEqual(sc.hits,5)
		
	def test_get_sunk(self):
		sc = Ship('Carrier', [3,4], 6, 'H')
		
		self.assertEqual(sc.get_sunk(), False)
		
		
	def test_hits(self):
		sc = Ship('Carrier', [4,8], 2, 'V')
		
		sc.inc_hits()
		self.assertEqual(sc.hits,1)
		
